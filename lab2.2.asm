.data
var1: .word 0x10   # reserve space in memory for varibles and asign values 
var2: .word 0x20
var3: .word 0x30
var4: .word 0x40
first: .byte 'b'
last: .byte 'h'

.text
.globl main
main: addu $s0,$ra, $0

#exchange values for var1 and var4
lw $t0, var1  # load the value of var1 into $t0
lw $t8, var4  # load the value of var4 into er
sw $t0, var4 # put the contant in $t0 (the value of var1) into var4, and get new var4 
sw $t8, var1 # put the contant in $t8 (the value of var4) into var1, and get new var1

#exchange values for var2 and var3
lw $t0, var2  # load the value of var2 into $t0
lw $t8, var3  # load the value of var3 into er
sw $t0, var3 # put the contant in $t0 (the value of var2) into var3, and get new var3 
sw $t8, var2 # put the contant in $t8 (the value of var3) into var2, and get new var2

#check the displacement of first and last
lb $t0, first
lb $t0, last

addu $ra,$0, $s0
jr $ra
##end the program
#li $v0,10  
#syscall


