.data 0x10010000
# reserve space in memory for varibles and asign values 
var1: .word 41   
var2: .word 28
.extern ext1 4
.extern ext2 4

.text
.globl main
main: addu $s0,$ra, $0


#exchange values for var1 and var4
lw $t0, ext1  #load ext1 so that we can check its displacement from $gp
lw $t0, ext2  #load ext1 so that we can check its displacement from $gp
lw $t0, var1  # load the value of var1 into $t0
lw $t8, var2  # load the value of var4 into $t8
sw $t0, ext2 # put the contant in $t0 (the value of var1) into ext2
sw $t8, ext1 # put the contant in $t8 (the value of var2) into ext1

addu $ra,$0, $s0
jr $ra


