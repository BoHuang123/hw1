
# coding: utf-8

# In[92]:

import time
import random


# In[93]:

#algorithm1  row in the inner loop
def multiplybh1(leftM, rightM):
    
    leftM_col=len(leftM[0])
    leftM_row=len(leftM)
    rightM_col=len(rightM[0])
    rightM_row=len(rightM)
    #print (leftM_col,leftM_row,rightM_col,rightM_row)
    if leftM_col!=rightM_row:
            print (leftM_col,rightM_row,"Wrong input!")
    else:
        t1=[]
        t2=[]
        t3=[]
        tt1=[]
        tt2=[]
        for q in range(rightM_col):
            t2=[]
            for j in range(leftM_row):
                t1=[]
                for i in range(rightM_row):
                    t1.append(rightM[i][q]*leftM[j][i])
                t2.append(sum(t1))
            t3.append(t2)
        #print(len(t3),len(t3[0]))    
        for i in range(len(t3[0])):
            tt1=[]
            for j in range(len(t3)):
                tt1.append(t3[j][i])
            tt2.append(tt1)
        #print(tt2)   
        return(tt2)
    
#algorithm2  column in the inner loop
def multiplybh2(leftM, rightM):
    
    leftM_col=len(leftM[0])
    leftM_row=len(leftM)
    rightM_col=len(rightM[0])
    rightM_row=len(rightM)
    #print (leftM_col,leftM_row,rightM_col,rightM_row)
    if leftM_col!=rightM_row:
            print (leftM_col,rightM_row,"Wrong input!")
    else:
        t1=[]
        t2=[]
        t3=[]
        tt1=[]
        tt2=[]
        for q in range(leftM_row):
            t2=[]
            for j in range(rightM_col):
                t1=[]
                for i in range(rightM_row):
                    t1.append(rightM[i][j]*leftM[q][i])
                t2.append(sum(t1))
            t3.append(t2)
        #print(t3)
        #print(len(t3),len(t3[0]))    
        for i in range(len(t3[0])):
            tt1=[]
            for j in range(len(t3)):
                tt1.append(t3[j][i])
            tt2.append(tt1)
        #print(tt2)   
        return(t3)


# In[94]:

def GenM(flag,Srow,Scol,Ecol,start,end): #flag=="int" or flag != "int"
    #Srow=3
    #Scol=4
    Erow=Scol
    #Ecol=5
    #start=1
    #end=8
    Sarr=[]
    Earr=[]
    t1=[]
    for i in range(Srow):
        t1=[]
        for j in range(Scol):
            if flag=="int":
                t1.append(random.randint(start,end))
                
            else:
                t1.append(random.uniform(start,end))
                
        Sarr.append(t1)
    #print(Sarr)

    for i in range(Erow):
        t1=[]
        for j in range(Ecol):
            if flag=="int":
                t1.append(random.randint(start,end))
            else:
                t1.append(random.uniform(start,end))
        Earr.append(t1)
   # if flag=="int":
        #print("Data: int")
   # else:
        #print("Data: double")
    return(Sarr,Earr)


# In[102]:

def runtest(count,method,mleft,mright,flag,ParamforGenM):
    timeArr=[]
            
    if method==1:
        notes="row in the inner loop"
    else:
        notes="column in the inner loop"
        
    for n in range(count):
        if len(mleft)*len(mright)==0:
            if flag=="int":
                mleft,mright=GenM("int",ParamforGenM[0],ParamforGenM[1],ParamforGenM[2],ParamforGenM[3],ParamforGenM[4])
            else:
                mleft,mright=GenM("float",ParamforGenM[0],ParamforGenM[1],ParamforGenM[2],ParamforGenM[3],ParamforGenM[4])
        else:
                flag=""
        t0=time.time()
        if method==1:
            outm=multiplybh1(mleft, mright)
        else:
            outm=multiplybh2(mleft, mright)
        deltaTime=time.time()-t0
        timeArr.append(deltaTime)
    print("Using algorithm:",method," ",notes)
    print("time=",sum(timeArr)/len(timeArr))
    print("repeat=",len(timeArr))
    print("----------------------")
    return(outm,sum(timeArr)/len(timeArr),len(timeArr))



#MAIN START FROM HERE
#generate matrices 
#a,b=GenM("int",3,4,5,1,8) means to generate 3X4 and 4X5 with all the value as int and between 1 to 8
#aMint,bMint=GenM("int",130,140,300,1,1000)
#aMf,bMf=GenM("float",130,140,300,1,1000)
#print("Data generated!")
#print("****************")
#print("")
    
#run several rounds of tests
#runtest(50,1,a,b,"int") means to run 50 rounds of test with algorithm 1, with a anb b as input matrices
roundsU=30
CtrlofGM=[330,240,300,1,1000]

print("Test with int")
print("")

outputM=runtest(roundsU,1,"","","int",CtrlofGM)
outputM=runtest(roundsU,2,"","","int",CtrlofGM)
print("")

print("Test with double")
print("")
outputM=runtest(roundsU,1,"","","float",CtrlofGM)
outputM=runtest(roundsU,2,"","","float",CtrlofGM)