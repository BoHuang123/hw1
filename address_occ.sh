#!/bin/bash
cut -d " " -f 2 $1 | perl -lpe '$_=hex' | paste -d " "  $1 - | sort  --field-separator=" " --key=3 -n | cut -d " " -f 2 | uniq -c > $1.result