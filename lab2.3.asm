.data 0x10010000
var1: .word 0x10   # reserve space in memory for varibles and asign values 
var2: .word 0x20
var3: .word 0x30
var4: .word 0x40
first: .byte 'b'
last: .byte 'h'

.text
.globl main
main: addu $s0,$ra, $0
#nop
#exchange values for var1 and var4

#lw $t0, var1  # load the value of var1 into $t0
lui $1, 4097
lw $t0, 0($1)

#lw $t8, var4  # load the value of var4 into $t8
#lui $1, 4097
lw $t8, 12($1)

#sw $t0, var4 # put the contant in $t0 (the value of var1) into var4, and get new var4 
#lui $1, 4097
sw $t0, 12($1)

#sw $t8, var1 # put the contant in $t8 (the value of var4) into var1, and get new var1
#lui $1, 4097
sw $t8, 0($1)

#exchange values for var2 and var3

#lw $t0, var2  # load the value of var2 into $t0
#lui $1, 4097
lw $t0, 4($1)

#lw $t8, var3  # load the value of var3 into er
#lui $1, 4097
lw $t8, 8($1)

#sw $t0, var3 # put the contant in $t0 (the value of var2) into var3, and get new var3 
#lui $1, 4097
sw $t0, 8($1)

#sw $t8, var2 # put the contant in $t8 (the value of var3) into var2, and get new var2
#lui $1, 4097
sw $t8, 4($1)

#check the displacement of first and last

#lb $t0, first
#lui $1, 4097
lb $t0, 16($1)

#lb $t0, last
#lui $1, 4097
lb $t0, 17($1)

addu $ra,$0, $s0
jr $ra
##end the program
#li $v0,10  
#syscall


